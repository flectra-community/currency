# Flectra Community / currency

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[currency_rate_update](currency_rate_update/) | 2.0.1.3.0| Update exchange rates using OCA modules
[currency_rate_update_cmc](currency_rate_update_cmc/) | 2.0.1.0.0| Allows to download crypto currency exchange rates from Coin Market Cap
[currency_rate_inverted](currency_rate_inverted/) | 2.0.1.0.2| Allows to maintain an exchange rate using the inversion method
[account_cryptocurrency](account_cryptocurrency/) | 2.0.1.0.1| Manage cryptocurrencies


